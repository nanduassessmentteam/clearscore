# This is developed in MVP pattern to keep the UI view and data model loosely coupled
# This is implemented using Kotlin
# I have used the okhttp3 to make connection to the end-point
# Retrofit is not needed as its only a single end-point
# Used native JSON for parsing the JSON object as we need only one particular value to be displayed in this demo
# The UI is not matching 100 % with he requirement
