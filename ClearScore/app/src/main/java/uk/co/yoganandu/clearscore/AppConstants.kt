package uk.co.yoganandu.clearscore

/**
 * Created by yoganandu on 04/03/2018.
 * This file holds all the constants used across the module
 */
class AppConstants{
    companion object {
        const val SCORE_ENDPOINT = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod/mockcredit/values"
        const val CREDIT_REPORT_INFO_JSON_KEY = "creditReportInfo"
        const val CREDIT_SCORE_JSON_KEY = "score"
        const val TOTAL_CREDIT_SCORE = 700
    }
}