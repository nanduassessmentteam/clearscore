package uk.co.yoganandu.clearscore

import android.app.Activity
import android.content.Context
import okhttp3.*
import java.io.IOException
import org.json.JSONObject


/**
 * Created by yoganandu on 03/03/2018.
 * This is the presenter class which holds the logic and makes UI calls accordingly
 * @constructor creates presenter
 * @param mScoreUI view instance to update the score
 * @param mContext activity context instance
 */
class ScorePresenter(private val mScoreUI: ScoreContract.ScoreUI, private val mContext: Context) : ScoreContract.ScoreActionListener{

    /**
     *This method fetches the credit score by making HTTP call to the given end-point
     * And makes corresponding UI calls based on the call response.
     */
    override fun fetchScore() {
        val httpClient= OkHttpClient()
        val request: Request = Request.Builder().url(AppConstants.SCORE_ENDPOINT).build()
        httpClient.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call?, e: IOException?) {
                sendError("UNKNOWN error", "Please try after some time")
            }

            override fun onResponse(call: Call?, response: Response?) {

                val resCode = response?.code()
                if(resCode == 200){

                    /*
                     *Only the required data is extracted from JSON based ont he ask to display
                     * only the credit score and the donut chart.
                     */
                    val responseJSONStr : String? = response?.body()?.string()
                    val jsonObject = JSONObject(responseJSONStr)

                    val creditRportInfoJSONObject = jsonObject.getJSONObject(AppConstants.CREDIT_REPORT_INFO_JSON_KEY)

                    val creditScore = creditRportInfoJSONObject.getInt(AppConstants.CREDIT_SCORE_JSON_KEY)

                    val activity = mContext as Activity
                    activity.runOnUiThread(object: Runnable{
                        override fun run() {
                            mScoreUI.showCreditScore(creditScore, AppConstants.TOTAL_CREDIT_SCORE)
                        }
                    })
                }else {
                    sendError("HTTP error", "Please try after some time")
                }

            }

        })
    }

    /**
     * It sends error message to the UI
     * @param errorTitle title of the error
     * @param errorMessage detailed error message
     */
    private fun sendError(errorTitle:String, errorMessage: String) {
        val activity = mContext as Activity
        activity.runOnUiThread(object: Runnable{
            override fun run() {
                mScoreUI.showError(errorTitle, errorMessage)
            }
        })
    }

}