package uk.co.yoganandu.clearscore

/**
 * Created by yoganandu on 03/03/2018.
 */

interface ScoreContract{

    /*
     * ScoreUI is the view interface
     */
    interface ScoreUI{
        fun showCreditScore(creditScore: Int, totalCreditScore: Int)
        fun showError(errorTitle: String, errorMessage: String)
    }

    /*
     * ScoreActionListener is the presenter interface
     */
    interface ScoreActionListener{
        fun fetchScore()
    }
}