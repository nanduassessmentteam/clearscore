package uk.co.yoganandu.clearscore

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_dash_board.*

/**
 * Created by yoganandu on 04/03/2018.
 */

class DashBoardActivity : AppCompatActivity(), ScoreContract.ScoreUI {

    private var mDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        supportActionBar?.title = getString(R.string.dashboard_title)
        val scorePresenter: ScoreContract.ScoreActionListener = ScorePresenter(this, this)
        setUpProgressDialog()
        showProgressDialog()
        scorePresenter.fetchScore()
    }

    /**
     * This method sets up the progress dialog to be shown while the HTTP call is happening
     */
    private fun setUpProgressDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setView(R.layout.score_loading_progress_dialog)
        builder.setCancelable(false)
        mDialog = builder.create()
    }

    /**
     * This method shows the progress dialog
     */
    private fun showProgressDialog(){
        mDialog?.show()
    }
    /**
     * This method dismisses the progress dialog
     */
    private fun hideProgressDialog(){
        mDialog?.dismiss()
    }

    /**
     * This method populates the credit score text and the donut chart as well
     * @param creditScore is the actual credit score
     * @param totalCreditScore is the total credit score used to determine the credit score percentage
     */
    override fun showCreditScore(creditScore: Int, totalCreditScore: Int) {
        txtScore.text = String.format(getString(R.string.creditscore_text), creditScore.toString())
        scoreProgressbar.progress = creditScore * 100 / totalCreditScore
        hideProgressDialog()
    }

    override fun showError(errorTitle: String, errorMessage: String) {
        hideProgressDialog()
        Toast.makeText(this, errorTitle + ":" + errorMessage, Toast.LENGTH_SHORT).show()
    }

}
